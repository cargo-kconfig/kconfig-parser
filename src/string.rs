/*
 Cargo KConfig - KConfig parser
 Copyright (C) 2022  Sjoerd van Leent

--------------------------------------------------------------------------------

Copyright Notice: Apache

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

   https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

--------------------------------------------------------------------------------

Copyright Notice: GPLv2

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------

Copyright Notice: MIT

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//! Additional String helper functions

/// Escapes a given value into an escaped string
pub fn escape(value: &str) -> String {
    format!(
        "\"{}\"",
        value
            .chars()
            .flat_map(|c| match c {
                '"' => vec!['\\', '"'],
                '\n' => vec!['\\', 'n'],
                '\r' => vec!['\\', 'r'],
                '\t' => vec!['\\', 't'],
                '\\' => vec!['\\', '\\'],
                _ => vec![c],
            })
            .collect::<String>()
    )
}

/// Parses a string value and removes escapes
pub fn parse(value: &str) -> Result<String, String> {
    let mut chars = value.chars();
    // Trim the left and right double quotes
    if value.len() < 2 || chars.next() != Some('\"') || chars.last() != Some('\"') {
        return Err(format!(
            "Could not find starting or ending double quote in: {}",
            value
        ));
    }
    let basic = value[1..value.len() - 1].chars();

    // Build the string into result
    let mut result: String = "".to_string();

    // If the state is Normal, then it is a normal character, which is appended
    // into result, if the state is Escape, then an escape character (\) has been
    // found prior to the current character, and the character needs unescaping.
    enum State {
        Normal,
        Escaped,
    }
    let mut state = State::Normal;
    for c in basic {
        match state {
            State::Normal => match c {
                '\\' => state = State::Escaped,
                _ => result += &format!("{}", c),
            },
            State::Escaped => {
                match c {
                    '"' => result += "\"",
                    'n' => result += "\n",
                    'r' => result += "\r",
                    't' => result += "\t",
                    '\\' => result += "\\",
                    _ => return Err(format!("Expected escape symbol, found: {}", c)),
                };
                state = State::Normal;
            }
        }
    }
    // If the state remained into escaped, it means that the last character has not
    // been properly closed.
    if let State::Escaped = state {
        return Err("Expected input to be closed, found escape token: \\".to_string());
    }
    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_escape_and_parse_string() {
        let escaped = escape("foo\"\n\r\t\\bar");
        let parsed = parse(&escaped).unwrap();

        assert_eq!("foo\"\n\r\t\\bar", parsed);
        assert_eq!("\"foo\\\"\\n\\r\\t\\\\bar\"", escaped);
    }

    #[test]
    fn test_wrong_escape() {
        assert_eq!(true, parse("\"\\w\"").is_err());
    }

    #[test]
    fn test_attempt_parse_without_opening_quote() {
        assert_eq!(true, parse("a\"").is_err());
    }

    #[test]
    fn test_attempt_parse_without_closing_quote() {
        assert_eq!(true, parse("\"a").is_err());
    }

    #[test]
    fn test_did_not_end_escape() {
        assert_eq!(true, parse("\"\\\"").is_err());
    }
}
