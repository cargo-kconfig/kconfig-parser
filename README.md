# Kconfig parser - for Cargo Kconfig

This is the parser for Kconfig menu items, as used by the Linux Kernel and other
projects. It is primarly created to be used by the Cargo Kconfig package, which
is still under heavy development, but you are free to use it for your purposes
if you like. If you want an easier interface, then don´t use this package, but
use Kconfig-represent instead, which uses this parser, but implements the logic
for reading and writing .config files, evaluating the definitions, etc. This
package merely produces an AST from Kconfig.

When using the package, note that there are three lexers, which are stapled on
top of one another, in the following order:

- The base lexer is used for reading a stream of bytes (this would typically be
  a file, but another stream is feasible to implement)
- The source lexer is used to read subsequent streams of bytes defined by the
  source keyword in a Kconfig file
- The macro lexer is used for interpreting macros and generating a symbol table
  for assigned values
